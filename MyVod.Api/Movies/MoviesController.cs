using System;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyVod.Api.Movies.Resources;
using MyVod.Domain.Movies.Commands;

namespace MyVod.Api.Movies
{
    [ApiController]
    [Route("api/[controller]")]
    public class MoviesController : ControllerBase
    {
        private readonly ISender _sender;

        public MoviesController(ISender sender)
        {
            _sender = sender;
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateMovieResource resource)
        {
            var result = await _sender.Send(new CreateMovieCommand(resource.Title, resource.Description));
            
            return result.Match<IActionResult>(success: data => Created(new Uri($"/api/movies/{data.Value}", UriKind.Relative), null),
                error: error => error switch
                {
                    _ => StatusCode(StatusCodes.Status500InternalServerError)
                });
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(/*MovieId id*/)
        {
            return Ok();
        }

        [HttpPost("{id}/publish")]
        public async Task<IActionResult> Publish(/*MovieId id*/)
        {
            return Ok();
        }
    }
}