namespace MyVod.Api.Movies.Resources
{
    public class CreateMovieResource
    {
        public string Title { get; set; }
        public string Description { get; set; }
    }

    public class MovieResource : CreateMovieResource
    {
        //public MovieId Id { get; set; }
        
        public static MovieResource From(/*Movie movie*/)
            => new MovieResource
            {
            };
    }
}