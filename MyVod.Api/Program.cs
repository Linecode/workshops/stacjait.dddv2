using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace MyVod.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }

    // public class User
    // {
    //     public UserId Id { get; set; }
    // }
    //
    // public readonly struct UserId {}
    //
    // public readonly struct RoleId {}
    //
    //
    // public class Role
    // {
    //     public RoleId Id { get; set; }
    // }
    //
    //
    // public class Service
    // {
    //     public void Action(UserId userId, RoleId roleId)
    //     {}
    //
    //     public static void Run()
    //     {
    //         var user = new User();
    //         var role = new Role();
    //         var service = new Service();
    //         service.Action(user.Id, role.Id);
    //     }
    // }
}