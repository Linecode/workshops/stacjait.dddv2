using System;
using System.Threading.Tasks;
using Linecode.DDD;
using MyVod.Domain.Movies;
using MyVod.Domain.Movies.Repositories;

namespace MyVod.Infrastructure.Movies
{
    public class MoviesRepository : IMoviesRepository
    {
        private readonly MoviesContext _context;

        public IUnitOfWork UnitOfWork => _context;

        public MoviesRepository(MoviesContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public async Task<Movie> Get(MovieId id)
        {
            return await _context.Movies.FindAsync(id);
        }

        public void Add(Movie movie)
            => _context.Add(movie);
    }
}