using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MyVod.Domain.Movies;

namespace MyVod.Infrastructure.Movies
{
    public class MovieEntityTypeConfiguration : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id)
                .HasConversion(id => id.Value, s => new MovieId(s));

            builder.Property(x => x.Title);
            builder.Property(x => x.Description);
            builder.Property(x => x.Status)
                .HasConversion<string>();

            builder.Property(x => x.Timestamp)
                .IsRowVersion();

            builder.OwnsMany(x => x.PricingProfiles, p =>
            {
                p.HasKey("MovieId", "Code");

                p.Property(x => x.Code)
                    .HasConversion(x => x.Value, x => new PricingProfileCode(x));

                p.OwnsOne(x => x.Price, y =>
                {
                    y.Property(x => x.Currency)
                        .HasConversion<string>()
                        .HasColumnName("Price_Currency");

                    y.Property(x => x.Value)
                        .HasColumnName("Price_Value")
                        .HasPrecision(10, 2);
                });
                
                p.ToTable("MoviesPricingProfiles", MoviesContext.Schema);
            });

            builder.Ignore(x => x.DomainEvents);
            builder.ToTable("Movies", MoviesContext.Schema);
        }
    }
}