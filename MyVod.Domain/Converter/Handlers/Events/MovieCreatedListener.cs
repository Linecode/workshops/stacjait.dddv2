using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Logging;
using MyVod.Domain.Movies;

namespace MyVod.Domain.Converter.Handlers.Events
{
    public class MovieCreatedListener : INotificationHandler<MovieCreated>
    {
        private readonly ILogger<MovieCreatedListener> _logger;

        public MovieCreatedListener(ILogger<MovieCreatedListener> logger)
        {
            _logger = logger;
        }
        
        public async Task Handle(MovieCreated notification, CancellationToken cancellationToken)
        {
            await Task.Delay(2000);
            _logger.LogInformation($"Movie converted: {notification.Movie.Id}");
        }
    }
}