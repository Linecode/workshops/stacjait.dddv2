using System.Collections.Generic;

namespace MyVod.Domain.Shared
{
    public class User
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        
        public IEnumerable<string> Groups { get; set; } = new List<string>();
    }
}