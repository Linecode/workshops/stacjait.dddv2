using System.Collections.Generic;
using Linecode.DDD;

namespace MyVod.Domain.Shared
{
    public class Money : ValueObject<Money>
    {
        public decimal Value { get; }
        public Currencies Currency { get; }

        public Money(decimal value, Currencies currency)
        {
            Value = value;
            Currency = currency;
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
            yield return Currency;
        }

        public enum Currencies
        {
            PLN,
            USD,
            EUR
        }
    }
}