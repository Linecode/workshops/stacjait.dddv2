using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Linecode.DDD;
using Linecode.DDD.Extensions;
using MyVod.Domain.Movies.Policies.Buy;
using MyVod.Domain.Movies.Repositories;
using MyVod.Domain.Shared;

namespace MyVod.Domain.Movies
{
    public sealed class Movie : Aggregate<MovieId>
    {
        public string Title { get; private set; }
        public string Description { get; private set; }
        public MovieStatus Status { get; private set; }

        private List<PricingProfile> _pricingProfiles = new();
        public IReadOnlyCollection<PricingProfile> PricingProfiles => _pricingProfiles;

        public bool IsPublished => Status == MovieStatus.Published;
        
        public byte[] Timestamp { get; private set; }

        [Obsolete("Only for EF Core", true)]
        private Movie(){}
        
        internal Movie(string title, string description)
        {
            Title = title;
            Description = description;

            Status = MovieStatus.Created;
            Id = MovieId.New();
            
            Raise(new MovieCreated(this));
        }

        public Result CanUserBuy(User user, BuyPolicy policy)
            => policy.Buy(this, user);

        internal void Apply(Events.AddPricingProfileEvent @event)
        {
            if (_pricingProfiles.NotContains(@event.PricingProfile))
                _pricingProfiles.Add(@event.PricingProfile);
        }

        internal void Apply(Events.PublishEvent @event)
        {
            if (string.IsNullOrWhiteSpace(Title))
                throw new Exception();

            if (string.IsNullOrWhiteSpace(Description))
                throw new Exception();

            if (!_pricingProfiles.Any())
                throw new Exception();

            Status = MovieStatus.Published;
        }

        internal static class Events
        {
            internal record AddPricingProfileEvent(PricingProfile PricingProfile);

            internal record PublishEvent();
        }

        public static Movie New(string title, string description)
        {
            if (string.IsNullOrWhiteSpace(title))
                throw new ArgumentNullException(nameof(title));

            return new Movie(title, description);
        }

        public enum MovieStatus
        {
            Created,
            Published
        }
    }
}