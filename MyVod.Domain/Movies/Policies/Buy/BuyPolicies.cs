using System;
using System.Linq;
using Linecode.DDD;
using MyVod.Domain.Shared;

namespace MyVod.Domain.Movies.Policies.Buy
{
    public static class BuyPolicies
    {
        public static BuyPolicy Default = new CompositeBuyCheck(new PublishCheck(), new UserGroupCheck());
        public static BuyPolicy Vip = new VipGroupCheck();
        
        private class VipGroupCheck : BuyPolicy
        {
            public Result Buy(Movie movie, User user)
                => user.Groups.Contains("Vip") ? Result.Success() : Result.Error(new Exception());
        }

        private class PublishCheck : BuyPolicy
        {
            public Result Buy(Movie movie, User user)
                => movie.IsPublished ? Result.Success() : Result.Error(new Exception());
        }

        private class UserGroupCheck : BuyPolicy
        {
            public Result Buy(Movie movie, User user)
                => movie.PricingProfiles.Select(x => x.Code.Value)
                    .Any(_ => user.Groups.Contains(_))
                    ? Result.Success()
                    : Result.Error(new Exception());
        }
        
        private class CompositeBuyCheck : BuyPolicy
        {
            private readonly BuyPolicy[] _buyPolicies;

            public CompositeBuyCheck(params BuyPolicy[] buyPolicies)
            {
                _buyPolicies = buyPolicies;
            }

            public Result Buy(Movie movie, User user)
                => _buyPolicies.Select(x => x.Buy(movie, user))
                    .Where(x => !x.IsSuccessful)
                    .DefaultIfEmpty(Result.Success())
                    .First();
        }
    }
}