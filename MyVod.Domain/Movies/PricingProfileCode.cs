using System;
using System.Collections.Generic;
using Linecode.DDD;
using Linecode.DDD.Extensions;

namespace MyVod.Domain.Movies
{
    public class PricingProfileCode : ValueObject<PricingProfileCode>
    {
        private static readonly IEnumerable<string> Codes = new[] {"AA", "BB", "CC"};
        
        public string Value { get; private set; }

        public PricingProfileCode(string value)
        {
            if (Codes.NotContains(value))
                throw new ArgumentOutOfRangeException();
                
            Value = value;
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Value;
        }

        public static PricingProfileCode AA => new PricingProfileCode("AA");
        public static PricingProfileCode BB => new PricingProfileCode("BB");
        public static PricingProfileCode CC => new PricingProfileCode("CC");
    }
}