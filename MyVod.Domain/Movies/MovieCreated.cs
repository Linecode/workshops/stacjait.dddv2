using MediatR;

namespace MyVod.Domain.Movies
{
    public record MovieCreated(Movie Movie) : INotification;
}