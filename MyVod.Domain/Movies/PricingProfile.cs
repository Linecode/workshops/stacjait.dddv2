using System;
using System.Collections.Generic;
using Linecode.DDD;
using MyVod.Domain.Shared;

namespace MyVod.Domain.Movies
{
    public class PricingProfile : ValueObject<PricingProfile>
    {
        public Money Price { get; private set; }
        public PricingProfileCode Code { get; private set; }

        [Obsolete("Only for EF Core", true)]
        private PricingProfile()
        {
        }

        public PricingProfile(Money price, PricingProfileCode code)
        {
            Price = price;
            Code = code;
        }

        protected override IEnumerable<object> GetEqualityComponents()
        {
            yield return Price;
            yield return Code;
        }

        public static PricingProfile GetBasedOnCode(PricingProfileCode code)
        {
            if (code == PricingProfileCode.AA)
                return new PricingProfile(new Money(10, Money.Currencies.EUR), code);
            
            if (code == PricingProfileCode.BB)
                return new PricingProfile(new Money(20, Money.Currencies.EUR), code);
            
            if (code == PricingProfileCode.CC)
                return new PricingProfile(new Money(10, Money.Currencies.PLN), code);

            throw new ArgumentOutOfRangeException();
        }
    }
}