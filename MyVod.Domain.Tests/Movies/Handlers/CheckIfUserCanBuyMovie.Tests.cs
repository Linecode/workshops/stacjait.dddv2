using System.Threading;
using System.Threading.Tasks;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Linecode.DDD.Exceptions;
using Moq;
using MyVod.Domain.Movies;
using MyVod.Domain.Movies.Commands;
using MyVod.Domain.Movies.Handlers;
using MyVod.Domain.Movies.Repositories;
using MyVod.Domain.Shared;
using Xunit;

namespace MyVod.Domain.Tests.Movies.Handlers
{
    public abstract class CheckIfUserCanBuyMovieTests
    {
        public class WhenHandling : CheckIfUserCanBuyMovieTests
        {
            private readonly IFixture _fixture;
            private IMoviesRepository _repository;
            private CheckIfUserCanBuyMovieHandler _sut;

            public WhenHandling()
            {
                _fixture = new Fixture().Customize(new AutoMoqCustomization());
                _repository = _fixture.Freeze<IMoviesRepository>();
                _sut = _fixture.Create<CheckIfUserCanBuyMovieHandler>();
            }

            [Fact]
            public async Task It_should_return_error_result_if_movie_is_not_found()
            {
                Mock.Get(_repository).Setup(x => x.Get(It.IsAny<MovieId>()))
                    .ReturnsAsync((Movie) null);
                var command = new CheckIfUserCanBuyMovieCommand(MovieId.New(), new User());

                var result = await _sut.Handle(command, CancellationToken.None);

                result.IsSuccessful.Should().BeFalse();
                result.Exception.Should().BeOfType<NotFoundException>();
            }

            [Fact]
            public async Task It_should_allow_to_buy_vip_user_event_if_movie_is_not_published()
            {
                var movie = new Movie("test", "test");
                var user = new User {Groups = new[] {"Vip"}};
                Mock.Get(_repository).Setup(x => x.Get(It.IsAny<MovieId>()))
                    .ReturnsAsync(movie);
                var command = new CheckIfUserCanBuyMovieCommand(MovieId.New(), user);

                var result = await _sut.Handle(command, CancellationToken.None);

                result.IsSuccessful.Should().BeTrue();
                result.Data.Should().BeTrue();
            }
        }
    }
}