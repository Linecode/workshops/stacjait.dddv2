using FluentAssertions;
using MyVod.Domain.Movies;
using MyVod.Domain.Shared;
using Xunit;

namespace MyVod.Domain.Tests.Movies
{
    public class MovieAssert
    {
        private readonly Movie _movie;

        public MovieAssert(Movie movie)
        {
            _movie = movie;
        }

        public MovieAssert SetTitleAndDescription(string title, string description)
        {
            _movie.Title.Should().Be(title);
            _movie.Description.Should().Be(description);
            
            return this;
        }
    }
    
    public abstract class MovieTests
    {
        public class WhenCreating : MovieTests
        {
            [Fact]
            public void It_should_set_title_and_description()
            {
                var title = "Die Hard";
                var description = "Great Movie! 3/5";

                var movie = new Movie(title, description);

                var movieShould = new MovieAssert(movie);
                movieShould.SetTitleAndDescription(title, description);
            }
        }
    }
}